﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO;
using System.IO.IsolatedStorage;



namespace HitAndMatch
{
    public partial class MainPage : PhoneApplicationPage
    {
        int secretNum = 0;
        int numTries = 0;
        IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            restart();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            int buttonVal = Convert.ToInt32(b.Content);

            if (textBox1.Text == ("Enter a 4-digit number"))
            {
                textBox1.Text = "";
            }

            textBox1.Text = textBox1.Text + buttonVal.ToString();
            if (textBox1.Text.Length > 3)
            {
                if (checkNum(textBox1.Text))
                {
                    restart();
                }
                textBox1.Text = "";
            }
        }

        private Boolean checkNum(string argString)
        {
            int arg = Convert.ToInt32(argString);
            if (numTries == 0)
            {
                listBox1.Items.Clear();
            }
            if (arg == secretNum)
            {
                numTries++;
                textBlock1.Text = "#Guesses = " + numTries;
                listBox1.Items.Add("You got the secret number: " + argString);
                listBox1.ScrollIntoView(listBox1.Items.Last());
           
                return true;
            }
            else
            {
                int hit = 0;
                int match = 0;
                int snfocus = 0;
                int argfocus = 0;
                for (int i = 0; i < 4; i++)
                {
                    int sntemp = secretNum;
                    int argtemp = arg;
                    for (int j = 0; j <= i; j++)
                    {
                        snfocus = sntemp % 10;
                        argfocus = argtemp % 10;
                        sntemp = sntemp / 10;
                        argtemp = argtemp / 10;
                    }
                    if (snfocus == argfocus)
                    {
                        match++;
                    }
                    else
                    {
                        int argTemp2 = arg;
                        for (int k = 0; k < 4; k++)
                        {
                            argfocus = argTemp2 % 10;
                            argTemp2 = argTemp2 / 10;
                            if (argfocus == snfocus)
                            {
                                hit++;
                                break;
                            }
                        }
                    }
                }
                if (listBox1.Items.Contains(argString + ": " + hit + "hits, " + match + "matches."))
                {
                    return false;
                }
                else
                {
                    numTries = numTries + 1;
                    textBlock1.Text = "#Guesses = " + numTries;
                    listBox1.Items.Add(argString + ": " + hit + "hits, " + match + "matches.");
                    listBox1.ScrollIntoView(argString + ": " + hit + "hits, " + match + "matches.");
                }
                return false;
            }
        }
        private void restart()
        {
            //listBox1.Items.Clear();
            secretNum = 0;
            numTries = 0;
            //textBlock1.Text = "#Guesses = 0";
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            for (int i = 0; i < 4; i++)
            {
                bool repeated = true;
                while(repeated){
                    int sndigit = rand.Next(10);
                    if (digitOf(sndigit, secretNum))
                    {
                        repeated = true;
                    }
                    else
                    {
                        repeated = false;
                        secretNum = secretNum * 10 + sndigit;
                    }
                }
            }
        }

        private bool digitOf(int needle, int haystack)
        {
            while (haystack > 0)
            {
                int hay = haystack % 10;
                haystack = haystack / 10;
                if (needle == hay)
                {
                    return true;
                }
            }
            return false;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}